import sys
sys.path.append("/home/hhousseinho/MIAT/Documents")

from Bio import SeqIO
from DNABERT_Model.motif_utils import seq2kmer

k = 6  # Remplacez par la taille de k-mer que vous voulez utiliser

# Liste des noms de fichiers FASTA et leurs étiquettes de classe correspondantes
fichiers_fasta = [('SNORD_RFAM.fasta', 0), ('TRNA_RFAM_Reducted.fasta', 1)]

for fichier, classe in fichiers_fasta:
    # Ouvrez chaque fichier FASTA, convertissez les séquences en k-mers et écrivez-les dans un nouveau fichier
    for seq_record in SeqIO.parse(fichier, 'fasta'):
        # Convertit la séquence en k-mers
        kmer_sequence = seq2kmer(str(seq_record.seq), k)

        # Écrit les k-mers dans un fichier tsv
        # Le nouveau fichier aura le même nom que l'original, avec '_kmers' ajouté
        with open(fichier.replace('.fasta', '_kmers.tsv'), 'a') as f:
            # Écriture de la séquence de k-mers et de l'étiquette de classe correspondante, séparées par une tabulation
            f.write(f'{kmer_sequence}\t{classe}\n')

