
# DNABERT_Model

In this project, we're exploring the use of the DNABERT model, fine-tuning it to our specific data sets. The main goal is to classify two families of ncRNA, SNORD and TRNA, by transforming them into DNA in order to operate with the DNABERT model.

Please note that, to ensure the proper functioning of the DNABERT model, the data as well as certain functions are not separated into different files. This implies that they all must be located in the same directory to be executed correctly.

I want to clarify that some folders could not be shared here on GitLab due to their considerable size. This means that to run the DNABERT model, I use files of such a large size that I unfortunately can't share them here.

However, I provide here the most important files, which are essential for the execution of the DNABERT model.