
import sys
sys.path.append("/home/hhousseinho/MIAT/Documents")

from Bio import SeqIO
from DNABERT_Model.motif_utils import seq2kmer

k = 6  # Remplacez par la taille de k-mer que vous voulez utiliser

import pandas as pd

# Chemins d'accès aux fichiers de k-mers
kmers_files = ['SNORD_RFAM_kmers.fasta', 'TRNA_RFAM_Reducted_kmers.fasta']

# Initialisation d'un DataFrame vide pour stocker les données
data = pd.DataFrame(columns=['sequence', 'label'])

# Parcourir chaque fichier de k-mers et ajouter les données au DataFrame
for kmers_file in kmers_files:
    label = 0 if "SNORD" in kmers_file else 1  # Assigner la valeur de l'étiquette en fonction du nom du fichier
    with open(kmers_file, 'r') as f:
        lines = f.readlines()
        sequences = [line.strip() for line in lines[1::2]]  # Récupérer les séquences de k-mers à partir des lignes impaires
        df = pd.DataFrame({'sequence': sequences, 'label': label})
        data = pd.concat([data, df])

# Sauvegarder le DataFrame fusionné au format TSV
data.to_csv('train.tsv', sep='\t', index=False)
