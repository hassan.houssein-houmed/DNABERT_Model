import sys
sys.path.append("/home/hhousseinho/MIAT/Documents")

from Bio import SeqIO
from DNABERT_Model.motif_utils import seq2kmer

k = 6  # Remplacez par la taille de k-mer que vous voulez utiliser

# Liste des noms de fichiers FASTA
fichiers_fasta = ['SNORD_RFAM.fasta', 'TRNA_RFAM_Reducted.fasta']

for fichier in fichiers_fasta:
    # Ouvrez chaque fichier FASTA, convertissez les séquences en k-mers et écrivez-les dans un nouveau fichier
    for seq_record in SeqIO.parse(fichier, 'fasta'):
        # Convertit la séquence en k-mers
        kmer_sequence = seq2kmer(str(seq_record.seq), k)

        # Écrit les k-mers dans un nouveau fichier
        # Le nouveau fichier aura le même nom que l'original, avec '_kmers' ajouté
        with open(fichier.replace('.fasta', '_kmers.fasta'), 'a') as f:
            f.write(f'>{seq_record.id}\n{kmer_sequence}\n')

