import pandas as pd
import torch
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score, cohen_kappa_score, roc_auc_score, confusion_matrix
from transformers import AutoTokenizer, AutoModelForSequenceClassification, TrainingArguments, Trainer
import sys
sys.path.append("/home/hhousseinho/MIAT/Documents")

# Chemin d'accès au fichier TSV contenant les séquences et les étiquettes
data_file = '/home/hhousseinho/Documents/DNABERT_Model/train.tsv'

# Chemin d'accès au répertoire de sauvegarde des métriques
metrics_file = '/chemin/vers/votre/fichier/metrics.csv'

# Charger les données depuis le fichier TSV
data = pd.read_csv(data_file, sep='\t', engine='python')

# Diviser les données en ensembles d'entraînement, de validation et de test
train_val_data, test_data = train_test_split(data, test_size=0.2, random_state=42)
train_data, val_data = train_test_split(train_val_data, test_size=0.2, random_state=42)

# Initialiser le tokenizer et le modèle DNABERT
tokenizer = AutoTokenizer.from_pretrained('/home/hhousseinho/Documents/DNABERT_Model/3-new-12w-0')
model = AutoModelForSequenceClassification.from_pretrained('/home/hhousseinho/Documents/DNABERT_Model/3-new-12w-0')

# Encoder les séquences d'ADN avec le tokenizer
train_encoded_inputs = tokenizer(list(train_data['sequence']), truncation=True, padding=True)
val_encoded_inputs = tokenizer(list(val_data['sequence']), truncation=True, padding=True)
test_encoded_inputs = tokenizer(list(test_data['sequence']), truncation=True, padding=True)

# Créer un Dataset à partir des données encodées et des étiquettes
class CustomDataset(torch.utils.data.Dataset):
    def __init__(self, encoded_inputs, labels):
        self.encoded_inputs = encoded_inputs
        self.labels = labels

    def __getitem__(self, idx):
        item = {key: torch.tensor(val[idx]) for key, val in self.encoded_inputs.items()}
        item['labels'] = torch.tensor(self.labels.iloc[idx])
        return item

    def __len__(self):
        return len(self.labels)

train_dataset = CustomDataset(train_encoded_inputs, train_data['label'])
val_dataset = CustomDataset(val_encoded_inputs, val_data['label'])
test_dataset = CustomDataset(test_encoded_inputs, test_data['label'])

# Définir les arguments d'entraînement et le Trainer
training_args = TrainingArguments(
    output_dir='/home/hhousseinho/Documents/DNABERT_Model/finetuned_model',
    num_train_epochs=10,
    per_device_train_batch_size=8,
    per_device_eval_batch_size=8,
    logging_steps=500,
    save_steps=2000,
    evaluation_strategy='epoch',
    report_to='wandb',
    seed=42
)

trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=train_dataset,
    eval_dataset=val_dataset
)

# Ajouter les métriques
def compute_metrics(eval_pred):
    labels = eval_pred.label_ids
    preds = eval_pred.predictions.argmax(-1)
    accuracy = accuracy_score(labels, preds)
    error_rate = 1 - accuracy
    recall = recall_score(labels, preds)
    precision = precision_score(labels, preds)
    f1 = f1_score(labels, preds)
    cohen_kappa = cohen_kappa_score(labels, preds)
    roc_auc = roc_auc_score(labels, eval_pred.predictions[:, 1])
    confusion = confusion_matrix(labels, preds)

    # Créer un DataFrame pandas avec les métriques
    metrics_df = pd.DataFrame({
        'accuracy': [accuracy],
        'error_rate': [error_rate],
        'recall': [recall],
        'precision': [precision],
        'f1': [f1],
        'cohen_kappa': [cohen_kappa],
        'roc_auc': [roc_auc]
    })

    # Enregistrer le DataFrame des métriques dans un fichier CSV
    metrics_df.to_csv('/home/hhousseinho/Documents/DNABERT_Model/dnabert_metric.csv', index=False)

    return {
        'accuracy': accuracy,
        'error_rate': error_rate,
        'recall': recall,
        'precision': precision,
        'f1': f1,
        'cohen_kappa': cohen_kappa,
        'roc_auc': roc_auc,
        'confusion_matrix': confusion
    }

trainer.compute_metrics = compute_metrics

# Lancer l'entraînement du modèle
trainer.train()


